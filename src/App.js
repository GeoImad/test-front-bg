import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { MainLayout, Header, Footer } from "./components/layout";
import { Products, About, Home, Login ,Categorie , Panier ,Register} from "./components/pages/index";

import "./App.css";

function App() {
  return (
    <div>
      <BrowserRouter>
        <MainLayout header={<Header />} footer={<Footer />}>
          <Switch>
            <Route path={"/about"} exact component={About} />
            <Route path={"/products"} exact component={Products} />
            <Route path={"/login"} exact component={Login} />
            <Route path={"/Register"} exact component={Register} />
            <Route path={"/Categorie"} exact component={Categorie} />
            <Route path={"/Panier"} exact component={Panier} />
            <Route path={"/"} exact component={Home} />
          </Switch>
        </MainLayout>
      </BrowserRouter>
    </div>
  );
}

export default App;
