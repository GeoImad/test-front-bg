import React, { useState } from "react";
import "./Product.css";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import {DollarCircleOutlined} from '@ant-design/icons';
import Cookies from 'universal-cookie';
/*import test from img;*/



function Product({ id,title, prix ,desc,img}) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  
function panier() {
  var qnt=document.getElementById('qnt').value;
  console.log(qnt)
  const cookies = new Cookies();
var here =0;
  var paniers = cookies.get('panier');
  if(paniers === undefined){
     paniers = []
  }
  for (let i = 0; i < paniers.length; i++) {
  if(paniers[i].id === id)
  {
    here=1;
    break;
  }  
}
if(here===0){
  paniers.push({
    title : title,
    id : id,
    prix : prix,
    img : img,
    qnt : qnt 
  })

   cookies.set('panier', paniers);
   alert('votre produit a etes ajoute au panier')
}
else {
  alert('votre produit deja ajoute');
}
}
  return (
    <div id="test" className={"app-product"}>
      
      <Card style={{ width: '18rem' }}>
      <Card.Body onClick={handleShow}>
    <Card.Text>
  <Card.Title>{title}</Card.Title>
      </Card.Text>
  </Card.Body>

  <Card.Img variant="top"  id="img" src={img} />
  <Card.Body>
    <Card.Text>
    {prix} DH {img}
    </Card.Text>
    <Card.Text>
    <i class="arrow right"></i>
    
    </Card.Text>
  </Card.Body>
</Card>
<>
<form>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
  <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{desc}</Modal.Body>
        <Card.Img variant="top" id="img" src={img} />
        <Modal.Body>{prix}</Modal.Body>
        Quantité pour:<input type="number" min="1" placeholder='0' id='qnt' name='number'/>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" type='submit' onClick={panier}>
            Add to payment
          </Button>
        </Modal.Footer>
      </Modal></form>
    </>
    </div>
);
}



export default Product;
