import React from "react";
import Form from 'react-bootstrap/Form';
//import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import axios from "axios";
import './Register.css'


class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {nom: '',prenom: '',email: '' , password: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value});
  }
  
 async handleSubmit(event) {
  event.preventDefault();
  var data = JSON.stringify({"nom":this.state.nom,"prenom":this.state.prenom,"email":this.state.email,"password":this.state.password});
      await axios({
            method: 'POST',
            url: 'http://localhost:8000/register',
            data: data,
            headers: {
               'Authorization': `Bearer ${ localStorage.getItem("token")}` , 
              'Content-Type': 'application/json; charset=utf-8',
           }
        })
        .then(response => {
          if(response.status === 201){
            alert('vous avez bien registre click ok pour login')
            this.props.history.push('/login')        }
        })
        .catch(error => {
          alert('error')
          // data = error.response.data;
        });
       
}
  render() {

    return (

  <div id="formel">
    <div id="wew">
      Inscrivez-Vous :)
    </div>
<Form id="formel2">
  <Form.Group controlId="formBasicName">
    <Form.Label id="fl">Nom :</Form.Label>
    <Form.Control id="fi" type="text" name="nom" placeholder="Enter votre nom" value={this.state.nom} onChange={this.handleChange}/>
  </Form.Group>
  <Form.Group controlId="formBasicNam">
    <Form.Label id="fl">Prenom :</Form.Label>
    <Form.Control id="fi" type="text" name="prenom" placeholder="Enter votre prenom" value={this.state.prenom} onChange={this.handleChange}/>
  </Form.Group>
  <Form.Group controlId="formBasicEmail">
    <Form.Label id="fl">E-mail :</Form.Label>
    <Form.Control id="fi" type="email" name="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChange}/>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label id="fl">Password :</Form.Label>
    <Form.Control id="fi" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
  </Form.Group>
  <Button id="fi2" variant="primary" onClick={this.handleSubmit} type="button">
    GO !
  </Button>
</Form></div>
    );
  }

  }
  export default Register; 