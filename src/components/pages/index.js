import Products from "./Products";
import About from "./About";
import Home from "./Home";
import Login from "./Login"
import Categorie from "./Categorie"
import Panier from "./Panier";
import Register from "./Register"
export { Products, About, Home, Login,Categorie,Panier,Register};
