import React, { useState, useEffect , Component } from "react";
import Product from "../shared/Product";
import axios from "axios";
/*import Filter from "../layout/Filter"*/
import "./Products.css";
import TestFilter from "../layout/TestFilter"
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";

/*export default class Products extends Component {
  constructor(props){
    super(props);
    this.state={products:[], filteredProducts:[]};
    }
    componentWillMount(){
      fetch("http://localhost:3000/products").then(res.json())
      .then(data => this.setState({
        products: data,
        filteredProducts: data}
      ));
    }
  render() {
    return (
      <div>
        <Filter size={this.state.size} sort={this.state.sort} handleChangeSize={this.state.handleChangeSize} handleChangeSort={this.state.handleChangeSort} count={this.state.filteredProducts.length}/>
      </div>
    )
  }
}*/

var count2 = 0;


function Products({ className }) {
  const [products, setProducts] = useState([]);
  const [status, setStatus] = useState(initStatus);

  
  useEffect(function onMounted() {
    setStatus({
      value: "product-loading"
    });

    getProducts()
      .then(function onSuccess(products) {
        setProducts(products);
        setStatus({
          value: "product-loaded"
        });
      })
      .catch(function onError(error) {
        setStatus({
          value: "product-error",
          error: error
        });
      });
  }, []);

  async function getProducts() { //fonction json 
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );

    return data;
  }

  function buildProduct({ id, title, body }) {
    return <Product key={id} title={title} body={body} />;
  }

  function initStatus() {
    return {
      value: "none",
      error: null
    };
  }

  function cnt() {
    return {
      count2
    };
  }

  var content = null;

  if (status.value == "product-loading") {
    content = <div>Loading...</div>;
  }

  if (status.value == "product-loaded") {
    content = <div id="tes22t" >
      <hr/>
      
      <Button id="but1">
      <Link id="aaa"to={"/categories"}>Categories</Link>
      </Button>
      <Button id="but2" primary size='85'>
      <Link id="aaa"to={"/products"}>All Products</Link>
      </Button>
      <hr/>
    
    <div className={className}>
                {products.map(buildProduct)}
                

              </div></div>; //map = for
         
      
  }

  return content;
}

export default Products;
