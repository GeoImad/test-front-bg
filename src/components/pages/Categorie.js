import React, { useState, useEffect} from "react";
import Product from "../shared/Product";
import axios from "axios";
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import './Categorie.css';
import load from '../imgs/load.gif';

function Categorie({ className }) {
    const [products, setProducts] = useState([]);
    const [status, setStatus] = useState(initStatus);
  
    useEffect(function onMounted() {
      setStatus({
        value: "product-loading"
      });
      getProducts()
        .then(function onSuccess(products) {
          console.log(products.data);
          setProducts(products);
          setStatus({
            value: "product-loaded"
          });
        })
        .catch(function onError(error) {
          setStatus({
            value: "product-error",
            error: error
          });
        });
    }, []);
  
    async function getProducts(link) { //fonction json 
      const { data } = await axios({
        method: 'get',
        url: localStorage.getItem('categorie'),
       headers: {  'Authorization': `Bearer ${ localStorage.getItem("token")}`,
       'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
       }
      });
  
      return data;
    }
    function buildProduct({ id, libelle, prix_ttc,created_at,description }) {
      return <Product key={id} title={libelle} prix={prix_ttc} creat={created_at} desc={description} />;
    }
    function categorie(event)
    {
      localStorage.setItem("categorie",event );
    //this.setState({redirect: "/din"})
    }
    function initStatus() {
      return {
        value: "none",
        error: null
      };
    }
  
    var content = null;
  
    if (status.value === "product-loading") {
      content = <div><img id="loading" src={load}></img></div>;
    }
  
    if (status.value === "product-loaded") {
      content =  <div id="container">
      <DropdownButton id="dropdown-basic-button" title="Filter by">
       <Dropdown.Item onClick={() => categorie('http://localhost:8000/2din/produits')} href='Categorie'>2 din</Dropdown.Item>
       <Dropdown.Item onClick={() => categorie('http://localhost:8000/camera-retro/produits')}  href="Categorie">camera retro</Dropdown.Item>
       <Dropdown.Item onClick={() => categorie('http://localhost:8000/radiobox/produits')}  href="Categorie">radiobox</Dropdown.Item>
</DropdownButton>
<hr>
</hr>
      {products.data.map((buildProduct))}
      </div>
      }
    return content;
  }
export default Categorie