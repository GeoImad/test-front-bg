import React from "react";
import Cookies from 'universal-cookie';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import './Panier.css';

var cookies = new Cookies();
const Panier =() => {
 function handleClick(id) {
  var pan = cookies.get('panier');
  for (let i = 0; i < pan.length; i++) {
    if(pan[i].id === id) pan.splice(i,1)
  }
cookies.set('panier',pan);
 } 
var pani= null ;

  var pan = cookies.get('panier');
  var prixtotal =0;
  var qnt;
  for (let i = 0; i < pan.length; i++) {
    pan[i].qnt == 0? qnt=1:qnt=pan[i].qnt ;
prixtotal += (pan[i].prix*qnt);
  }
 pani= prixtotal
 

 console.log(pani)
  return (  
    <span > 
      
      <div id="confirm">Total prix des produits : {pani} DH</div> 
      
      <Button id="conf" variant="primary" type="submit">Confirm achat</Button>
      <hr></hr>
    {cookies.get('panier').map(function (Item,i) {
    //  var idd = {Item.id}
     return <div >
     <div  id="pan" key={i} style={{ width: '18rem' }}>
     
     <Card.Body>
       
    <Card.Title id="tit">{Item.title}</Card.Title>
    <Card.Img variant="top" src={Item.img} />
       <Card.Text>
         {Item.prix} DH
       </Card.Text>
       <Card.Text>
         QUANTITE :{Item.qnt}
       </Card.Text>
       <Button id="suppr" variant="primary" type="submit" onClick={() => handleClick(Item.id)} href='/panier' >Supprimer produit</Button>
     </Card.Body>
     
   </div>
   <hr id="hrs">
   </hr>
   </div>
    })} 
           
    </span>
  );
}

export default Panier;