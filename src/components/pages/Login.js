import React from "react";
import Form from 'react-bootstrap/Form';
//import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import axios from "axios";
import './Login.css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {email: '' , password: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value});
  }
  
 async handleSubmit(event) {
  event.preventDefault();
  var data = JSON.stringify({"email":this.state.email,"password":this.state.password});
    await axios({
            method: 'POST',
            url: 'http://localhost:8000/login',
            data: data,
            headers: {
               'Authorization': `Bearer ${ localStorage.getItem("token")}` , 
              'Content-Type': 'application/json; charset=utf-8',
           }
        })
        .then(response => {
          if(response.status === 200){
            localStorage.setItem("user",response.data.nom);
            this.props.history.push('/')        }
        })
        .catch(error => {
          alert('user or password incorrect')
          // data = error.response.data;
        });
       
}
  render() {

    return (

  
<div id="formela">
    <div id="connectez">
      Connectez-Vous :)
    </div>
<Form id="formela2">
  <Form.Group controlId="formBasicEmail">
    <Form.Label id="fl">E-mail :</Form.Label>
    <Form.Control id="fi" type="email" name="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChange}/>
    <Form.Text id="fl" className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label id="fl">Password :</Form.Label>
    <Form.Control id="fi" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
  </Form.Group>
  <Form.Group id="check" controlId="formBasicCheckbox">
    <Form.Check  type="checkbox" label="Souvenir de moi" />
  </Form.Group>
  <Button id="sub" variant="primary" onClick={this.handleSubmit} type="button">
    GO !
  </Button>
</Form></div>
    );
  }

  }
  export default Login; 