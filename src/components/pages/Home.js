import React, {Component} from 'react';
import { Link } from "react-router-dom";
import {
    Container,
    Divider,
    Grid,
    Header,
    Icon,
    Image,
    List,
    Menu,
    Segment,
    Visibility,
} from 'semantic-ui-react';
import Button from 'react-bootstrap/Button';
import Products from "./Products";
import "./Home.css";
import "../imgs/home.jpg"

function Home({ className }) {
        return ( 
             
            <Segment id="all"
                inverted
                textAlign='center'
                style={{ minHeight: "88.5vh" ,    }}
                vertical
                
            >
                <div1 id='backdiv' style={{zIndex:1}}>

                </div1>

                <div id='chark'>
                CHARK Accessoires
                </div>
                <div id='chark2'>
                You can find whatever you want for your car.
                </div>
                <div2 id='cont'> 
                <Button primary size='huge' id='butt'>
                    <Link id="a" to={"/products"}>Products</Link>
                        
                    </Button>
                    <Button primary size='huge' id='butt2'>
                    <Link id="a" to={"/about"}>About</Link>
                        
                    </Button>
                    <Button primary size='huge' id='butt3'>
                    <Link id="a" to={"/login"}>Login</Link>
                        
                    </Button>
                </div2>
                <Container text style={{zIndex:6}}>
                    
                    
                </Container>
                
                
            </Segment>
            
        );
    
}

export default Home;