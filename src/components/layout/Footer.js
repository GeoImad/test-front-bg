import React from "react";
import "./footer.css";
import {  MDBContainer } from "mdbreact";

const FooterPage = () => {
  return (
    <div id="bg">
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid id="text">
          &copy; {new Date().getFullYear()} Copyright
        </MDBContainer>
      </div>
    </div>
  );
}

export default FooterPage;