import React, { Component } from 'react'
import Filter from './Filter'

export default class testFilter extends Component {
    constructor(props){
        super(props);
        this.state={products:[], filteredProducts:[]};
        }
        componentWillMount(){
          fetch("http://localhost:3000/products").then(res => res.json())
          .then(data => this.setState({
            products: data,
            filteredProducts: data}
          ));
        }
    render() {
        return (
            <div>
                
                <Filter size={this.state.size} sort={this.state.sort} handleChangeSize={this.state.handleChangeSize} handleChangeSort={this.state.handleChangeSort} count={this.state.filteredProducts.length}/>
            </div>
        )
    }
}
