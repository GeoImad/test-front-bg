import React from "react";

function MainLayout({ className = "", header, children, footer }) {
  return (
    <div className={`app-layout ${className}`}>
      <header className={"app-layout-header"}>{header}</header>
      <main className={"app-layout-content"}>{children}</main>
      <footer className={"app-layout-footer"}>{footer}</footer>
    </div>
  );
}

export default MainLayout;
